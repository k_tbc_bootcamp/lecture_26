package com.lkakulia.lecture_26

interface CustomCallback {
    fun onFailure(error: String?)
    fun onResponse(response: String)
}