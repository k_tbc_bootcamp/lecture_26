package com.lkakulia.lecture_26

import com.google.gson.annotations.SerializedName

class UserModel(
    var id: Int = 0,
    var email: String = "",
    @SerializedName("first_name")
    var firstName: String = "",
    @SerializedName("last_name")
    var lastName: String = "",
    var avatar: String = ""
) {

}