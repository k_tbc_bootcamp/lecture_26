package com.lkakulia.lecture_26

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

abstract class BaseFragment: Fragment() {

    protected lateinit var itemView: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
            itemView = inflater.inflate(getLayoutResource(), container, false)
            start(inflater, container, savedInstanceState)
            return itemView
    }

    abstract fun getLayoutResource(): Int

    abstract fun start(inflater: LayoutInflater,
                       container: ViewGroup?,
                       savedInstanceState: Bundle?)
}