package com.lkakulia.lecture_26

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_user.view.*

class UserFragment(private val user: UserModel, private val pageCount: Int) : BaseFragment() {

    override fun getLayoutResource(): Int {
        return R.layout.fragment_user
    }

    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        init()
    }

    private fun init() {
        Glide.with(itemView.context).load(user.avatar).into(itemView.avatarImageView)
        //I deliberately didn't use local variables or string resources to suppress the warnings
        itemView.idTextView.text = "id: " + user.id.toString()
        itemView.emailTextView.text = "email: ${user.email}"
        itemView.firstNameTextView.text = "first name: ${user.firstName}"
        itemView.lastNameTextView.text = "last name: ${user.lastName}"
        itemView.pageCountTextView.text = "page count: ${pageCount.toString()}"
    }

}
