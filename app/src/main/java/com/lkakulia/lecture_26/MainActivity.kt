package com.lkakulia.lecture_26

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject

/*
Uncomment/comment either adapter with its corresponding code to test functionality
 */

class MainActivity : AppCompatActivity() {

    private val users = mutableListOf<UserModel>()

    //fragment viewPager adapter
//    private lateinit var fragmentViewPagerAdapter: FragmentViewPagerAdapter

    //ordinary viewPager adapter
    private lateinit var viewPagerAdapter: ViewPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        HttpRequest.getRequest(HttpRequest.USERS, object: CustomCallback {
            override fun onFailure(error: String?) {
                Toast.makeText(this@MainActivity, error, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(response: String) {
                val jsonArray = JSONObject(response).getJSONArray("data")
                (0 until jsonArray.length()).forEach {
                    val jsonObject = jsonArray.get(it) as JSONObject
                    val user = Gson().fromJson(jsonObject.toString(), UserModel::class.java)
                    users.add(user)
                }

                //fragment viewPager
//                fragmentViewPagerAdapter = FragmentViewPagerAdapter(supportFragmentManager, users)
//                viewPager.adapter = fragmentViewPagerAdapter

                //ordinary viewPager
                viewPagerAdapter = ViewPagerAdapter(users)
                viewPager.adapter = viewPagerAdapter
                viewPager.offscreenPageLimit = 3
            }
        })
    }
}
