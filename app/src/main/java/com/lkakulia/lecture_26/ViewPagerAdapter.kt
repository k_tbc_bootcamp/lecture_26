package com.lkakulia.lecture_26

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_user.view.*

class ViewPagerAdapter(private val users: MutableList<UserModel>): PagerAdapter() {
    override fun isViewFromObject(view: View, itemView: Any): Boolean {
        return view == itemView
    }

    override fun getCount(): Int {
        return users.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = LayoutInflater.from(container.context).inflate(R.layout.fragment_user, container, false)
        val user = users[position]
        Glide.with(itemView.context).load(user.avatar).into(itemView.avatarImageView)
        //I deliberately didn't use local variables or string resources to suppress the warnings
        itemView.idTextView.text = "id: " + user.id.toString()
        itemView.emailTextView.text = "email: ${user.email}"
        itemView.firstNameTextView.text = "first name: ${user.firstName}"
        itemView.lastNameTextView.text = "last name: ${user.lastName}"
        itemView.pageCountTextView.text = "page count: ${position + 1}"
        container.addView(itemView)
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, itemView: Any) {
        container.removeView(itemView as View)
    }
}